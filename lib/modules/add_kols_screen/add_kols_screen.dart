import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/cubit/states.dart';

import '../../layout/cubit/cubit.dart';
import '../../models/kol_search_model.dart';
import '../../shared/components/components.dart';

class AddKolsScreen extends StatelessWidget {

  var searchController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        ConsultationCubit.get(context).getAllKols();
        return BlocConsumer<ConsultationCubit, ConsultationStates>(
          listener: (context, state){},
          builder: (context, state)
          {
            return Scaffold(
              appBar: AppBar(
                leading: const BackButton(
                  color: Colors.black,
                ),
                titleSpacing: 0,
                backgroundColor: const Color(0xFFBDBDBD),
                title: const Text(
                  'APP NAME',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                actions: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.more_vert,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              body: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    right: 15,
                    left: 15,
                    bottom: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Add KOLS to your case:',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const Text(
                        'Please search and select the KOLs whom you want to consult, then tap "ADD"',
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 45,
                        child: defaultFormField(
                          onChange: (dynamic value){
                            ConsultationCubit.get(context).filter(value);
                          },
                          controller: searchController,
                          suffix: const Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          type: TextInputType.text,
                          validate: (dynamic value) {
                            if (value!.isEmpty) {
                              return 'please enter KOL\'s name or speciality';
                            }
                          },
                          label: 'You can search with KOL name or speciality',
                        ),
                      ),
                      ListView.separated(
                        physics: const BouncingScrollPhysics(),
                        shrinkWrap: true,
                          itemBuilder: (context, index) => buildKolCaseItem(context, ConsultationCubit.get(context).results[index], index),
                          separatorBuilder: (context, index) => myDivider(),
                          itemCount: ConsultationCubit.get(context).results.length,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey[200]
                        ),
                        child: TextButton(
                          onPressed: ()
                          {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              CircleAvatar(
                                backgroundColor: Colors.grey,
                                radius: 8,
                                child: Icon(
                                  Icons.add,
                                  size: 15,
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(width: 5,),
                              Text(
                                'ADD',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),

                        ),
                      ),

                    ],
                  ),
                ),
              ),
            );
          },
        );
      }
    );
  }

  Widget buildKolCaseItem(context, KolSearchModel model, int index) => InkWell(
    onTap: ()
    {
      ConsultationCubit.get(context).kolSelected(model.isSelected, index);
    },
    child: Container(
      decoration: BoxDecoration(
        color: model.isSelected ? Colors.grey[400] : Colors.white,
        borderRadius: BorderRadius.circular(5.0,),
      ),

      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children:  [
             CircleAvatar(
              radius: 20.0,
              backgroundImage: NetworkImage(
                model.image,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
             Text(
              model.name,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Container(
              height: 22,
              width: 80,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(5.0,),
              ),
              child:  Center(
                child: Text(
                  model.speciality.toUpperCase(),
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 12,

                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    ),
  );



}

