import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/user_model.dart';
import 'hcp_states.dart';

class ConsultationHcpRegisterCubit extends Cubit<ConsultationHcpRegisterStates> {
  ConsultationHcpRegisterCubit() : super(ConsultationHcpRegisterInitialState());

  static ConsultationHcpRegisterCubit get(context) => BlocProvider.of(context);

  bool termsAndConditions = false;
  void changeTermsAndConditions()
  {
    termsAndConditions = !termsAndConditions;

    emit(ConsultationHcpRegisterChangeTermsState());
  }

  bool isPassword = true;

  void changePasswordVisibility()
  {
    isPassword = !isPassword;

    emit(ConsultationChangePasswordVisibilityState());
  }

  void hcpRegister({
    required String email,
    required String password,
    required String name,
    required bool conditions,
    required String role,
  })
  {
    emit(ConsultationHcpRegisterLoadingState());

    FirebaseAuth.instance
        .createUserWithEmailAndPassword(
      email: email,
      password: password,
    )
        .then((value)  {
      createHcpInfo(
        email: email,
        name: name,
        uId: value.user!.uid,
        conditions: conditions,
        role: role,
      );
      emit(ConsultationHcpRegisterSuccessState(value.user!.uid));
    }).catchError((error) {
      emit(ConsultationHcpRegisterErrorState(error.toString()));
    });
  }

  void createHcpInfo({
    required String email,
    required String uId,
    required String name,
    required bool conditions,
    required String role,
  })
  {
    UserModel userModel = UserModel(
      email: email,
      uId: uId,
      name: name,
      conditions: conditions,
      role: role,
      image: 'https://img.freepik.com/free-photo/smiling-doctor-sitting-isolated-grey_651396-917.jpg?w=996&t=st=1675946010~exp=1675946610~hmac=8a9bbd3bd839aba590944470052bd86320ec18f7ee489da1de38016113ae5c23',
        speciality: '',
    );

    emit(ConsultationHcpCreateInfoLoadingState());

    FirebaseFirestore.instance
        .collection('users')
        .doc(uId)
        .set(userModel.toMap())
        .then((value) {
      emit(ConsultationHcpCreateInfoSuccessState());

    }).catchError((error) {
      emit(ConsultationHcpCreateInfoErrorState(error.toString()));
    });
  }


}

