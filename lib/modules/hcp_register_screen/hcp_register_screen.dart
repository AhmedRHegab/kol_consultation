import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/hcp_home_page.dart';
import 'package:kol_consultation/modules/hcp_register_screen/cubit/hcp_cubit.dart';
import 'package:kol_consultation/modules/hcp_register_screen/cubit/hcp_states.dart';

import '../../shared/components/components.dart';
import '../../shared/components/constants.dart';
import '../../shared/network/local/cache_helper.dart';

class HcpRegisterScreen extends StatelessWidget {

  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var nameController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return  BlocProvider(
      create: (BuildContext context) => ConsultationHcpRegisterCubit(),
      child: BlocConsumer<ConsultationHcpRegisterCubit, ConsultationHcpRegisterStates>(
        listener: (context, state)
        {
          if(state is ConsultationHcpRegisterErrorState)
            {
              showToast(text: state.error.toString(), state: ToastStates.ERROR);
            }
          if(state is ConsultationHcpRegisterSuccessState)
            {
              CacheHelper.saveData(
                key: 'uId',
                value: state.uId,
              ).then((value) {
                uId = CacheHelper.getData(key: 'uId');
                navigateTo(context, HcpHomePage());
              });

            }
        },
        builder: (context, state)
        {
          return Scaffold(
            body: Form(
              key: formKey,
              child: ListView(
                children:
                [
                  Padding(
                    padding:  EdgeInsets.only(top: height*0.12),
                    child: Container(
                      height: MediaQuery.of(context).size.height*0.15,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          const Image(
                              color: Colors.orange,
                              image: AssetImage(
                                'assets/images/icons/doctor_icon.png',
                              )),
                          Positioned(
                            bottom: 0,
                            child: Row(
                              children: const [
                                Text(
                                  'KOL',
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black

                                  ),
                                ),
                                Text(
                                  ' Consultation',
                                  style: TextStyle(
                                      fontSize: 22,
                                      color: Colors.black54

                                  ),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height*0.06,
                  ),
                  const Center(
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 26,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height*0.05,
                  ),
                  Container(
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: defaultFormField(
                        controller: nameController,
                        type: TextInputType.name,
                        validate: (dynamic value)
                        {
                          if(value!.isEmpty)
                          {
                            return 'name is empty !';
                          }
                          return null;
                        },
                        label: 'name',
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: defaultFormField(
                        controller: emailController,
                        type: TextInputType.emailAddress,
                        validate: (dynamic value)
                        {
                          if(value!.isEmpty)
                          {
                            return 'email is empty !';
                          }
                          return null;
                        },
                        label: 'Email',
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: defaultFormField(
                        isPassword: ConsultationHcpRegisterCubit.get(context).isPassword,
                        controller: passwordController,
                        type: TextInputType.visiblePassword,
                        validate: (dynamic value)
                        {
                          if(value!.isEmpty)
                          {
                            return 'password is empty';
                          }
                          return null;
                        },
                        suffix: TextButton(
                          onPressed: ()
                          {
                            ConsultationHcpRegisterCubit.get(context).changePasswordVisibility();
                          },
                          child:  Text(
                            ConsultationHcpRegisterCubit.get(context).isPassword ? 'Show' : 'Hide',
                            style: const TextStyle(
                              color: Colors.orange,
                            ),
                          ),
                        ),
                        label: 'Password',
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: CheckboxListTile(
                      checkColor: Colors.grey[100],
                      activeColor: Colors.black,
                      value: ConsultationHcpRegisterCubit.get(context).termsAndConditions,
                      onChanged: (value) {
                        ConsultationHcpRegisterCubit.get(context).changeTermsAndConditions();
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                      title: const Text(
                        'I here by agree to the terms and conditions and acknowledge that i\'m a HCP',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(25.0,),
                      ),
                      child: TextButton(
                        onPressed: () {
                          if(formKey.currentState!.validate())
                          {
                            if(ConsultationHcpRegisterCubit.get(context).termsAndConditions)
                              {
                                ConsultationHcpRegisterCubit.get(context).hcpRegister(
                                  email: emailController.text,
                                  password: passwordController.text,
                                  name: nameController.text,
                                  conditions: ConsultationHcpRegisterCubit.get(context).termsAndConditions,
                                  role: 'hcp',
                                );
                              }else{
                              showToast(text: 'please accept terms and conditions', state: ToastStates.ERROR);
                            }
                          }
                        },
                        child: const Text(
                          'Sign Up',
                          style: TextStyle(
                            color: Colors.white,

                          ),
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
