import 'package:flutter/material.dart';

import '../../shared/components/components.dart';

class HcpCasesScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          ListView.separated(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) => buildKolCaseItem(context),
            separatorBuilder: (context, index) => myDivider(),
            itemCount: 15,
          ),
        ],
      ),
    );
  }

  Widget buildKolCaseItem(context) => InkWell(
    onTap: (){},
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:  [
          const CircleAvatar(
            radius: 20.0,
            backgroundImage: NetworkImage(
              'https://img.freepik.com/free-photo/smiling-doctor-sitting-isolated-grey_651396-917.jpg?w=996&t=st=1675946010~exp=1675946610~hmac=8a9bbd3bd839aba590944470052bd86320ec18f7ee489da1de38016113ae5c23',
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'Dr. Mohammed Kamel',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Case: 110-23',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ],
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width*0.18,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: const [
              Text(
                '12/10/2022',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              CircleAvatar(
                radius: 8.0,
                backgroundColor: const Color(0xFFBDBDBD),
                child: Text(
                  '3',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 10.0,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
