abstract class ConsultationLoginStates {}

class ConsultationLoginInitialState extends ConsultationLoginStates {}

class ConsultationChangePasswordVisibilityState extends ConsultationLoginStates {}

class KolConsultationLoginLoadingState extends ConsultationLoginStates {}

class KolConsultationLoginSuccessState extends ConsultationLoginStates
{
  final String uId;

  KolConsultationLoginSuccessState(this.uId);
}

class KolConsultationLoginErrorState extends ConsultationLoginStates
{
  final String error;

  KolConsultationLoginErrorState(this.error);
}

class ConsultationGetKolIdsLoadingState extends ConsultationLoginStates {}

class ConsultationGetKolIdsSuccessState extends ConsultationLoginStates {}


class ConsultationGetHcpIdsLoadingState extends ConsultationLoginStates {}

class ConsultationGetHcpIdsSuccessState extends ConsultationLoginStates {}
