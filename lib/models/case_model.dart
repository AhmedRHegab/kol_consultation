class CaseModel
{
  late String email;
  late String uId;
  late String name;
  late bool conditions;
  late String role;

  CaseModel({
    required this.email,
    required this.uId,
    required this.name,
    required this.conditions,
    required this.role,
  });

  CaseModel.fromJson(Map<String, dynamic> json)
  {
    email = json['email'];
    uId = json['uId'];
    name = json['name'];
    conditions = json['conditions'];
    role = json['role'];

  }

  Map<String, dynamic> toMap()
  {
    return {
      'email' : email,
      'uId' : uId,
      'name' : name,
      'conditions' : conditions,
      'role' : role,
    };
  }
}