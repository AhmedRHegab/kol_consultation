import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/cubit/cubit.dart';
import 'package:kol_consultation/layout/cubit/states.dart';
import 'package:kol_consultation/modules/kol_cases_screen/kol_cases_sccreen.dart';

import '../modules/kol_closed_cases_screen/closed_cases_screen.dart';

class KolHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 2,
        child: BlocConsumer<ConsultationCubit, ConsultationStates>(
          listener: (context, state) {},
          builder: (context, state)
          {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: const Color(0xFFBDBDBD),
                title: const Text(
                  'APP NAME',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                actions:  [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                  ),
                  IconButton(
                    onPressed: () { },
                    icon: const Icon(
                      Icons.more_vert,
                      color: Colors.black,
                    ),
                  ),
                ],
                bottom: const TabBar(
                  indicatorColor: Colors.black,
                  labelColor: Colors.black,
                  tabs: [
                    Tab(
                      text: 'Current Cases',
                    ),
                    Tab(
                      text: 'Closed',
                    ),
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  KolCasesScreen(),
                  KolClosedCasesScreen(),
                ],
              ),
            );
          },
        ),
      );


}
