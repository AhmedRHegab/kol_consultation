class KolSearchModel
{
  late String email;
  late String uId;
  late String name;
  late String image;
  late bool conditions;
  late String role;
  late String speciality;
  bool isSelected = false;

  KolSearchModel({
    required this.email,
    required this.uId,
    required this.name,
    required this.conditions,
    required this.role,
    required this.image,
    required this.speciality,
  });

  KolSearchModel.fromJson(Map<String, dynamic> json)
  {
    email = json['email'];
    uId = json['uId'];
    name = json['name'];
    conditions = json['conditions'];
    role = json['role'];
    image = json['image'];
    speciality = json['speciality'];

  }

  Map<String, dynamic> toMap()
  {
    return {
      'email' : email,
      'uId' : uId,
      'name' : name,
      'conditions' : conditions,
      'role' : role,
      'image' : image,
      'speciality' : speciality,
    };
  }
}