import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/cubit/cubit.dart';
import 'package:kol_consultation/shared/bloc_observer.dart';
import 'package:kol_consultation/shared/network/local/cache_helper.dart';
import 'firebase_options.dart';
import 'modules/kol_login/login_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await CacheHelper.init();
  Bloc.observer = MyBlocObserver();
  runApp( MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MultiBlocProvider(
      providers: [
        BlocProvider(create: (BuildContext context) => ConsultationCubit()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: AnnotatedRegion<SystemUiOverlayStyle>(
          value:   const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent, // transparent status bar
            statusBarIconBrightness: Brightness.dark, // status bar icons' color
            //systemNavigationBarIconBrightness: Brightness.dark, //navigation bar icons' color
          ),
          child: LoginScreen(),
        ),
      ),
    );

  }
}
