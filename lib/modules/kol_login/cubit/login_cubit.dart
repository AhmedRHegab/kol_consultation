import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_states.dart';

class ConsultationLoginCubit extends Cubit<ConsultationLoginStates> {
  ConsultationLoginCubit() : super(ConsultationLoginInitialState());

  static ConsultationLoginCubit get(context) => BlocProvider.of(context);

  bool isPassword = true;

  void changePasswordVisibility() {
    isPassword = !isPassword;

    emit(ConsultationChangePasswordVisibilityState());
  }

  void userLogin({
    required String email,
    required String password,
  }) {
    emit(KolConsultationLoginLoadingState());

    FirebaseAuth.instance
        .signInWithEmailAndPassword(
      email: email,
      password: password,
    )
        .then((value) {
      print(value.user?.email);
      print(value.user?.uid);
      emit(KolConsultationLoginSuccessState((value.user?.uid)!));
    }).catchError((error) {
      emit(KolConsultationLoginErrorState(error.toString()));
    });
  }

  List<String> kols = [];

  void getAllKols() {
    emit(ConsultationGetKolIdsLoadingState());
    FirebaseFirestore.instance
        .collection('users')
        .get()
        .then((event) {
      kols = [];
      event.docs.forEach((element) {
        kols.add(element.id);
      });
      emit(ConsultationGetKolIdsSuccessState());
    });
  }

  List<String> hcps = [];

  void getAllHcps() {
    emit(ConsultationGetHcpIdsLoadingState());
    FirebaseFirestore.instance
        .collection('hcp_users')
        .get()
        .then((event) {
      hcps = [];
      event.docs.forEach((element) {
        hcps.add(element.id);
      });
      emit(ConsultationGetHcpIdsSuccessState());
    });
  }

}
