abstract class ConsultationStates {}

class ConsultationInitialState extends ConsultationStates {}

class ConsultationSexSelectedState extends ConsultationStates {}

class ConsultationPickFileLoadingState extends ConsultationStates {}

class ConsultationPickFileSuccessState extends ConsultationStates {}

class ConsultationUploadFileLoadingState extends ConsultationStates {}

class ConsultationUploadFileSuccessState extends ConsultationStates {}

class ConsultationGetAllKolsLoadingState extends ConsultationStates {}

class ConsultationGetAllKolsSuccessState extends ConsultationStates {}

class ConsultationFilteringKolLoadingState extends ConsultationStates {}

class ConsultationFilteringKolSuccessState extends ConsultationStates {}

class ConsultationSelectingKolSuccessState extends ConsultationStates {}