import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/kol_home_page.dart';
import 'package:kol_consultation/modules/hcp_register_screen/hcp_register_screen.dart';

import '../../layout/hcp_home_page.dart';
import '../../shared/components/components.dart';
import '../../shared/network/local/cache_helper.dart';
import 'cubit/login_cubit.dart';
import 'cubit/login_states.dart';

class LoginScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
        return BlocProvider(
          create: (BuildContext context) => ConsultationLoginCubit(),
          child: BlocConsumer<ConsultationLoginCubit, ConsultationLoginStates>(
            listener: (context, state) {
              if (state is KolConsultationLoginSuccessState) {
                CacheHelper.saveData(
                  key: 'uId',
                  value: state.uId,
                ).then((value) {
                  route(context);
                });
              }
            },
            builder: (context, state) {
              return Scaffold(
                body: Form(
                  key: formKey,
                  child: ListView(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: height * 0.12),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              const Image(
                                  color: Colors.orange,
                                  image: AssetImage(
                                    'assets/images/icons/doctor_icon.png',
                                  )),
                              Positioned(
                                bottom: 0,
                                child: Row(
                                  children: const [
                                    Text(
                                      'KOL',
                                      style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      ' Consultation',
                                      style: TextStyle(
                                          fontSize: 22, color: Colors.black54),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: height * 0.10,
                      ),
                      const Center(
                        child: Text(
                          'Log In',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 26,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        height: height * 0.05,
                      ),
                      Container(
                        height: 50,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: defaultFormField(
                            controller: emailController,
                            type: TextInputType.emailAddress,
                            validate: (dynamic value) {
                              if (value!.isEmpty) {
                                return 'email is empty !';
                              }
                              return null;
                            },
                            label: 'Email',
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 50,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: defaultFormField(
                            isPassword:
                                ConsultationLoginCubit.get(context).isPassword,
                            controller: passwordController,
                            type: TextInputType.visiblePassword,
                            validate: (dynamic value) {
                              if (value!.isEmpty) {
                                return 'password is empty';
                              }
                              return null;
                            },
                            suffix: TextButton(
                              onPressed: () {
                                ConsultationLoginCubit.get(context)
                                    .changePasswordVisibility();
                              },
                              child: Text(
                                ConsultationLoginCubit.get(context).isPassword
                                    ? 'Show'
                                    : 'Hide',
                                style: const TextStyle(
                                  color: Colors.orange,
                                ),
                              ),
                            ),
                            label: 'Password',
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 80),
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(
                              20.0,
                            ),
                          ),
                          child: TextButton(
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                ConsultationLoginCubit.get(context).userLogin(
                                  email: emailController.text,
                                  password: passwordController.text,
                                );
                              }
                            },
                            child: const Text(
                              'Log In',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: TextButton(
                          onPressed: () {},
                          child: const Text(
                            'Forgot your password?',
                            style: TextStyle(
                                color: Colors.orange, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            'don\'t have an account?  ',
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextButton(
                            onPressed: ()
                            {
                              navigateTo(context, HcpRegisterScreen());
                            },
                            child: const Text(
                              'REGISTER NOW',
                              style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
          ),

        );

      }


  void route(context) {
    User? user = FirebaseAuth.instance.currentUser;
    var kk = FirebaseFirestore.instance
        .collection('users')
        .doc(user!.uid)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        if (documentSnapshot.get('role') == "kol") {
          navigateAndFinish(context, KolHomePage());
        }else{
          navigateAndFinish(context, HcpHomePage());
        }
      } else {
        print('Document does not exist on the database');
      }
    });
  }

}



