abstract class ConsultationHcpRegisterStates {}

class ConsultationHcpRegisterInitialState extends ConsultationHcpRegisterStates {}

class ConsultationHcpRegisterChangeTermsState extends ConsultationHcpRegisterStates {}

class ConsultationChangePasswordVisibilityState extends ConsultationHcpRegisterStates {}

class ConsultationHcpRegisterLoadingState extends ConsultationHcpRegisterStates {}

class ConsultationHcpRegisterSuccessState extends ConsultationHcpRegisterStates
{
  final String uId;

  ConsultationHcpRegisterSuccessState(this.uId);
}

class ConsultationHcpRegisterErrorState extends ConsultationHcpRegisterStates
{
  final String error;

  ConsultationHcpRegisterErrorState(this.error);
}

class ConsultationHcpCreateInfoLoadingState extends ConsultationHcpRegisterStates {}

class ConsultationHcpCreateInfoSuccessState extends ConsultationHcpRegisterStates {}

class ConsultationHcpCreateInfoErrorState extends ConsultationHcpRegisterStates {
  final String error;

  ConsultationHcpCreateInfoErrorState(this.error);
}