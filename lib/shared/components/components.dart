import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final Widget? suffix;

  CustomTextField({
    required this.hint, this.suffix,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0,),
      child: TextField(
        cursorColor: Colors.grey,
        decoration: InputDecoration(
          hintText: hint,
          suffixIcon: suffix,
          hintStyle: TextStyle(
            color: Colors.grey[400]
          ),
          filled: true,
          fillColor: Colors.grey[200],
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0,),
            borderSide: const BorderSide(
              color: Color(0xFFBDBDBD),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0,),
            borderSide: const BorderSide(
              color: Color(0xFFBDBDBD),
            ),
          ),
        ),
      ),
    );
  }
}


Widget defaultFormField({
  required TextEditingController controller,
  required TextInputType type,
  dynamic onSubmit,
  dynamic onTap,
  dynamic onChange,
  bool isClickable = true,
  bool isPassword = false,
  int maxLines = 1,
  required dynamic validate,
  required String label,
  Widget? prefix,
  Widget? suffix,
  dynamic suffixPressed,
}) => Padding(
  padding: const EdgeInsets.symmetric(horizontal: 0),
  child:   TextFormField(
    maxLines: maxLines,
    cursorColor: Colors.grey,
    style: const TextStyle(
      color: Colors.grey,
      overflow: TextOverflow.ellipsis,
    ),
    controller: controller,
    keyboardType: type,
    obscureText: isPassword,
    onFieldSubmitted: onSubmit,
    onChanged: onChange,
    onTap: onTap,
    enabled: isClickable,
    validator: validate,
    decoration: InputDecoration(
      hintText: label,
      suffixIcon: suffix,
      prefixIcon: prefix,
      hintStyle: TextStyle(
          color: Colors.grey[400],
        fontSize: 12,
      ),
      filled: true,
      fillColor: Colors.grey[200],
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0,),
        borderSide: const BorderSide(
          color: Color(0xFFEEEEEE),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0,),
        borderSide: const BorderSide(
          color: Color(0xFFEEEEEE),
        ),
      ),
    ),
  ),
);


void navigateTo(context, widget) => Navigator.push(context,
  MaterialPageRoute(
    builder: (context) => widget,
  ),
);

void navigateAndFinish(context, widget) => Navigator.pushAndRemoveUntil(context,
  MaterialPageRoute(
    builder: (context) => widget,
  ),
      (Route<dynamic> route) => false,
);


Widget myDivider() => Padding(
  padding: const EdgeInsetsDirectional.only(
    start: 20.0,
  ),
  child: Container(
    width: double.infinity,
    height: 1.0,
    color: Colors.grey[300],
  ),
);


void showToast({
  required String text,
  required ToastStates state,
}) =>  Fluttertoast.showToast(
    msg: text,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 5,
    backgroundColor: chooseToastColor(state),
    textColor: Colors.white,
    fontSize: 16.0
);

enum ToastStates {SUCCESS, ERROR, WARNING}

Color? chooseToastColor(ToastStates state)
{
  Color color;

  switch(state)
  {
    case ToastStates.SUCCESS:
      color = Colors.green;
      break;
    case ToastStates.ERROR:
      color = Colors.red;
      break;
    case ToastStates.WARNING:
      color = Colors.amber;
      break;
  }
  return color;
}