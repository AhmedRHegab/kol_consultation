import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/cubit/cubit.dart';
import 'package:kol_consultation/layout/cubit/states.dart';
import 'package:kol_consultation/modules/add_kols_screen/add_kols_screen.dart';
import 'package:kol_consultation/shared/components/components.dart';

import '../../models/kol_search_model.dart';

class AddNewCaseScreen extends StatelessWidget {
  var formKey = GlobalKey<FormState>();
  var patientController = TextEditingController();
  var ageController = TextEditingController();
  var heightController = TextEditingController();
  var weightController = TextEditingController();
  var diagnosisController = TextEditingController();
  var descriptionController = TextEditingController();
  var fileController = TextEditingController();


  List<String> sexList = [
    'male',
    'female',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ConsultationCubit, ConsultationStates>(
      listener: (context, state) {},
      builder: (context, state)
      {
        return Scaffold(
          appBar: AppBar(
            leading: const BackButton(
              color: Colors.black,
            ),
            titleSpacing: 0,
            backgroundColor: const Color(0xFFBDBDBD),
            title: const Text(
              'APP NAME',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            actions: [
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.search,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.more_vert,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                top: 20,
                right: 15,
                left: 15,
                bottom: 15,
              ),
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Add a new case:',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    const Text(
                      'please fill in the form and select the KOL name whom you want to consult',
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 45,
                      child: defaultFormField(
                        controller: patientController,
                        type: TextInputType.name,
                        validate: (dynamic value) {
                          if (value!.isEmpty) {
                            return 'please enter patient name';
                          }
                        },
                        label: 'Patient name...',
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Container(
                          height: 45,
                          width: 161,
                          child: defaultFormField(
                            controller: ageController,
                            type: TextInputType.number,
                            validate: (dynamic value) {
                              if (value!.isEmpty) {
                                return 'please enter patient age';
                              }
                            },
                            label: 'Age',
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          height: 45.0,
                          width: 161,
                          padding: EdgeInsets.only(left: 16, right: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              10.0,
                            ),
                            color: Colors.grey[200],
                          ),
                          child: DropdownButton(
                            hint: const Text(
                              'Sex',
                              style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFFBDBDBD),
                              ),
                            ),
                            isExpanded: true,
                            underline: const SizedBox(),
                            dropdownColor: Colors.grey,
                            icon: const Icon(Icons.arrow_drop_down),
                            iconDisabledColor: Colors.grey,
                            iconEnabledColor: Colors.grey,
                            iconSize: 32,
                            value: ConsultationCubit.get(context).sexSelected,
                            onChanged: (newValue)
                            {
                              ConsultationCubit.get(context).selectedSex(value: newValue);
                            },
                            items: sexList.map((sexValue) {
                              return DropdownMenuItem(
                                value: sexValue,
                                child: Text(sexValue),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Container(
                          height: 45,
                          width: 161,
                          child: defaultFormField(
                            controller: heightController,
                            type: TextInputType.number,
                            validate: (dynamic value) {
                              if (value!.isEmpty) {
                                return 'please enter patient height';
                              }
                            },
                            label: 'Height(CM)',
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          height: 45,
                          width: 161,
                          child: defaultFormField(
                            controller: weightController,
                            type: TextInputType.number,
                            validate: (dynamic value) {
                              if (value!.isEmpty) {
                                return 'please enter patient weight';
                              }
                            },
                            label: 'Weight(KG)',
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 45,
                      child: defaultFormField(
                        controller: diagnosisController,
                        type: TextInputType.text,
                        validate: (dynamic value) {
                          if (value!.isEmpty) {
                            return 'please enter patient diagnosis';
                          }
                        },
                        label: 'Your preliminary diagnosis...',
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: defaultFormField(
                        controller: descriptionController,
                        maxLines: 3,
                        type: TextInputType.text,
                        validate: (dynamic value) {
                          if (value!.isEmpty) {
                            return 'enter case description';
                          }
                        },
                        label: 'Case description...',
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey[200]
                        ),
                      child: TextButton(
                        onPressed: () async {
                          ConsultationCubit.get(context).pickFile();
                        },
                        child: Row(

                          children: const [
                            Icon(
                              Icons.file_copy,
                              size: 15,
                              color: Colors.grey,
                            ),
                            SizedBox(width: 10,),
                            Text(
                              'Attach files',
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),

                      )
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if(ConsultationCubit.get(context).selectedKols.isNotEmpty)
                        const Text(
                          'Selected KOLs:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                   ListView.separated(
                     shrinkWrap: true,
                       physics: const BouncingScrollPhysics(),
                       itemBuilder: (context, index) => buildSelectedKolItem(context, index, ConsultationCubit.get(context).selectedKols[index]),
                       separatorBuilder: (context, index) => myDivider(),
                       itemCount: ConsultationCubit.get(context).selectedKols.length,
                   ),

                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: double.infinity,
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey[200]
                      ),
                      child: TextButton(
                        onPressed: ()
                        {
                          navigateTo(context, AddKolsScreen());
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            CircleAvatar(
                              backgroundColor: Colors.grey,
                              radius: 8,
                              child: Icon(
                                Icons.add,
                                size: 15,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(width: 5,),
                            Text(
                              'ADD KOLS',
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),

                      ),
                    ),
                     SizedBox(
                      height: MediaQuery.of(context).size.height*0.082,
                    ),
                    Container(
                      width: double.infinity,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.green,
                      ),
                      child: TextButton(
                        onPressed: () {  },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.send,
                              size: 15,
                              color: Colors.white,
                            ),
                            SizedBox(width: 5,),
                            Text(
                              'SEND REQUEST',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildSelectedKolItem(context, int index, KolSearchModel model) => InkWell(
    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:  [
          Text(
              model.name,
          ),
          Text(
            model.speciality.toUpperCase(),
          ),
        ],
      ),
    ),
  );
}
