import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kol_consultation/layout/cubit/states.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:kol_consultation/shared/components/components.dart';
import '../../models/kol_search_model.dart';


class ConsultationCubit extends Cubit<ConsultationStates> {
  ConsultationCubit() : super(ConsultationInitialState());

  static ConsultationCubit get(context) => BlocProvider.of(context);

  String? sexSelected;

  void selectedSex({String? value}) {
    sexSelected = value!;
    emit(ConsultationSexSelectedState());
  }

  File? file;

  Future<void> pickFile() async {
    emit(ConsultationPickFileLoadingState());
    final path = await FlutterDocumentPicker.openDocument();
    print(path);
    file = File(path!);
     await uploadFile(file!);
    emit(ConsultationPickFileSuccessState());
  }

  Future<firebase_storage.UploadTask?> uploadFile(File file) async {
    emit(ConsultationUploadFileLoadingState());
    if (file == null) {
      showToast(text: 'no picked files', state: ToastStates.ERROR);
    } else {
      firebase_storage.UploadTask uploadTask;

      // Create a Reference to the file
      firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
          .ref()
          .child('files')
          .child('/some-file.pdf');

      final metadata = firebase_storage.SettableMetadata(
          contentType: 'file/pdf',
          customMetadata: {'picked-file-path': file.path});
      print("Uploading..!");

      uploadTask = ref.putData(await file.readAsBytes(), metadata);

      print("done..!");
      emit(ConsultationUploadFileSuccessState());
      return Future.value(uploadTask);
    }
  }

  List<KolSearchModel> kols = [];


  void getAllKols()
  {
    emit(ConsultationGetAllKolsLoadingState());
    FirebaseFirestore.instance.collection('users')
        .snapshots()
        .listen((event) {
       kols = [];
          event.docs.forEach((element) {
            if(element.data().containsValue('kol'))
              {
                kols.add(KolSearchModel.fromJson(element.data()));

              }
          });
          emit(ConsultationGetAllKolsSuccessState());
    });
  }

  List<KolSearchModel> kolResults = [];
  List<KolSearchModel> specialityResults = [];
  var results = [];

  void filter(String enteredKeyword)
  {
    emit(ConsultationFilteringKolLoadingState());
    if(enteredKeyword.isEmpty)
    {
      emit(ConsultationFilteringKolLoadingState());
      results = kols;
      emit(ConsultationFilteringKolSuccessState());
    }else{

      kolResults = (kols.where((user) => user.name.toLowerCase().contains(enteredKeyword.toLowerCase())).toList());
      specialityResults = (kols.where((user) => user.speciality.toLowerCase().contains(enteredKeyword.toLowerCase())).toList());
      results = [kolResults, specialityResults].expand((element) => element).toSet().toList();
      emit(ConsultationFilteringKolSuccessState());
    }

  }

  List<KolSearchModel> selectedKols = [];

  void kolSelected(bool isSelected,int index)
  {
   if(isSelected){
     results[index].isSelected = !isSelected;
     selectedKols.remove(results[index]);
     emit(ConsultationSelectingKolSuccessState());
   }else{
     results[index].isSelected = !isSelected;
     selectedKols.add(results[index]);
     selectedKols.toSet();
     emit(ConsultationSelectingKolSuccessState());
   }

  }

}

